<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/tournament/create', [PostController::class, 'create']);
Route::post('/tournament', [PostController::class, 'store']);

Route::resource('tournaments', 'App\Http\Controllers\TournamentController');
Route::resource('teams', 'App\Http\Controllers\TeamController');