<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tournament;
use App\Models\Team;
use Session;
use Redirect;
use View;


class TournamentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournaments = Tournament::all();
        return view('tournament.index')->with('tournaments', $tournaments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tournament.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $validated = $request->validate([
            'name'       => 'required',
            'start_date'      => 'required|date',
            'finish_date' => 'required|date|after:start_date'
        ]);
            // store
            $tournament = new tournament;
            $tournament->name       = $_REQUEST['name'];
            $tournament->start      = $_REQUEST['start_date'];
            $tournament->finish     = $_REQUEST['finish_date'];
            $tournament->save();

            // redirect
            Session::flash('message', 'Torneo creado exitosamente!');
            return Redirect::to('tournaments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get the tournament
        $tournament = Tournament::find($id);

        // show the view and pass the tournament to it
        return View::make('tournament.show')
            ->with('tournament', $tournament);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function teams($id)
    {
        $teams= Tournament::find($id)->teams;

        return View::make('team.show')
            ->with('teams', $teams);
    }

}
