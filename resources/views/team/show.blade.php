<div conference>
  <div bracket>
    <div class="round">
      <div class="game">
        <div class="team">
          Team A
        </div>
        <div class="team">
          Team B
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team C
        </div>
        <div class="team">
          Team D
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team E
        </div>
        <div class="team">
          Team F
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team G
        </div>
        <div class="team">
          Team H
        </div>
      </div>
    </div>
    <div class="round">
      <div class="game">
        <div class="team">
          Team A
        </div>
        <div class="team">
          Team D
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team F
        </div>
        <div class="team">
          Team H
        </div>
      </div>
    </div>
    <div class="round">
      <div class="game">
        <div class="team">
          Team A
        </div>
        <div class="team">
          Team F
        </div>
      </div>
    </div>
    <div class="round">
      <div class="game">
        <div class="team">
          Team A
        </div>
      </div>
    </div>
  </div>
  <div bracket="reverse">
    <div class="round">
      <div class="game">
        <div class="team">
          Team I
        </div>
        <div class="team">
          Team J
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team K
        </div>
        <div class="team">
          Team L
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team M
        </div>
        <div class="team">
          Team N
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team O
        </div>
        <div class="team">
          Team P
        </div>
      </div>
    </div>
    <div class="round">
      <div class="game">
        <div class="team">
          Team J
        </div>
        <div class="team">
          Team K
        </div>
      </div>
      <div class="game">
        <div class="team">
          Team N
        </div>
        <div class="team">
          Team O
        </div>
      </div>
    </div>
    <div class="round">
      <div class="game">
        <div class="team">
          Team K
        </div>
        <div class="team">
          Team O
        </div>
      </div>
    </div>
    <div class="round">
      <div class="game">
        <div class="team">
          Team O
        </div>
      </div>
    </div>
  </div>
</div>
<div note>
  <h2>Notes</h2>
  <p>
    This graph makes a few assumptions in order to render correctly. It assumes that you have a proper bracket that will reduce, and that each subsiquent <code>game</code> will resolve. Id est: Team count is <sup>2</sup>.
  </p>
</div>
