<!DOCTYPE html>
<html>
<head>
    <title>Tournament App</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('tournaments') }}">Ver Torneos</a></li>
    </ul>
</nav>

<h1>CREAR UN TORNEO</h1>

<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::open(array('url' => 'tournaments')) }}

    <div class="form-group">
        {{ Form::label('name', 'Nombre') }}
        {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('start_date', 'Fecha de Inicio') }}
        {{ Form::date('start_date', Request::old('start_date'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('finish_date', 'Fecha de Fin') }}
        {{ Form::date('finish_date', Request::old('finish_date'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('CREAR!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>