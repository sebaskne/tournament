<!DOCTYPE html>
<html>
<head>
    <title>Tournament App</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('tournaments') }}">Ver Torneos</a></li>
        <li><a href="{{ URL::to('tournaments/create') }}">Crear Torneo</a>
    </ul>
</nav>

<h1>TORNEOS</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Nombre</td>
            <td>Acciones</td>
        </tr>
    </thead>
    <tbody>
    @foreach($tournaments as $key => $value)
        <tr>
            <td>{{ $value->name }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the shark (uses the destroy method DESTROY /sharks/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->

                <!-- show the shark (uses the show method found at GET /sharks/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('tournaments/' . $value->id) }}">VER</a>

                <!-- edit this shark (uses the edit method found at GET /sharks/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('tournaments/' . $value->id . '/edit') }}">EDITAR</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
</body>
</html>