<!DOCTYPE html>
<html>
<head>
    <title>Tournament App</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('tournaments') }}">Ver Torneos</a></li>
        <li><a href="{{ URL::to('teams/create') }}">Crear Equipo</a>
    </ul>
</nav>

<h1>MOSTRANDO {{ $tournament->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $tournament->name }}</h2>
        <p>
            <strong>Fecha de Inicio:</strong> {{ $tournament->start }}<br>
            <strong>Fecha de Fin:</strong> {{ $tournament->finish }}
        </p>
    </div>


</div>
</body>
</html>