@extends('layouts.base')

@section('contenido')
<h2>CREAR TORNEO</h2>

<form action="/tournaments" method="POST">
    @csrf
    <div class="mb-3">
        <label for="" class="form-label">Nombre</label>
        <input id="name" name="name" type="text" class="form-control" tabindex="1">

        <?php echo $nameEmptyErr; ?>
    </div>
    <div class="mb-3">
        <label for="" class="form-label">Fecha de Inicio</label>
        <input id="start_date" name="start_date" type="date" class="form-control" tabindex="2">

        <?php echo $startEmptyErr; ?>
    </div>
    <div class="mb-3">
        <label for="" class="form-label">Fecha de Fin</label>
        <input id="end_date" name="end_date" type="date" class="form-control" tabindex="3">

        <?php echo $finishEmptyErr; ?>
        <?php echo $invalidDate; ?>
    </div>
    <a href="/tournaments" class="btn btn-secondary" tabindex="4">Cancelar</a>
    <button type="submit" class="btn btn-primary" tabindex="3">Guardar</button>
</form>
@endsection